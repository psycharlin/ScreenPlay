<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en">
<context>
    <name>Community</name>
    <message>
        <source>News</source>
        <translation>新闻</translation>
    </message>
    <message>
        <source>Wiki</source>
        <translation>维基</translation>
    </message>
    <message>
        <source>Forum</source>
        <translation>论坛</translation>
    </message>
    <message>
        <source>Contribute</source>
        <translation>贡献</translation>
    </message>
    <message>
        <source>Steam Workshop</source>
        <translation>Steam 创意工坊</translation>
    </message>
    <message>
        <source>Issue Tracker</source>
        <translation>问题跟踪器</translation>
    </message>
    <message>
        <source>Reddit</source>
        <translation>Reddit</translation>
    </message>
</context>
<context>
    <name>CommunityNavItem</name>
    <message>
        <source>Open in browser</source>
        <translation>在浏览器中打开</translation>
    </message>
</context>
<context>
    <name>CreateSidebar</name>
    <message>
        <source>Tools Overview</source>
        <translation type="unfinished">Tools Overview</translation>
    </message>
    <message>
        <source>Video Import h264 (.mp4)</source>
        <translation type="unfinished">Video Import h264 (.mp4)</translation>
    </message>
    <message>
        <source>Video Import VP8 &amp; VP9 (.webm)</source>
        <translation type="unfinished">Video Import VP8 &amp; VP9 (.webm)</translation>
    </message>
    <message>
        <source>Video import (all types)</source>
        <translation type="unfinished">Video import (all types)</translation>
    </message>
    <message>
        <source>GIF Wallpaper</source>
        <translation type="unfinished">GIF Wallpaper</translation>
    </message>
    <message>
        <source>QML Wallpaper</source>
        <translation type="unfinished">QML Wallpaper</translation>
    </message>
    <message>
        <source>HTML5 Wallpaper</source>
        <translation type="unfinished">HTML5 Wallpaper</translation>
    </message>
    <message>
        <source>Website Wallpaper</source>
        <translation type="unfinished">Website Wallpaper</translation>
    </message>
    <message>
        <source>QML Widget</source>
        <translation type="unfinished">QML Widget</translation>
    </message>
    <message>
        <source>HTML Widget</source>
        <translation type="unfinished">HTML Widget</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperInit</name>
    <message>
        <source>Import any video type</source>
        <translation>导入任何视频类型</translation>
    </message>
    <message>
        <source>Depending on your PC configuration it is better to convert your wallpaper to a specific video codec. If both have bad performance you can also try a QML wallpaper! Supported video formats are: 

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</source>
        <translation>取决于您的PC配置，最好将您的壁纸转换为特定的视频编码格式。如果它们的性能都不好，您还可以尝试 QML 壁纸！已支持的视频格式有：

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</translation>
    </message>
    <message>
        <source>Set your preffered video codec:</source>
        <translation>设置您偏好的视频编码格式：</translation>
    </message>
    <message>
        <source>Quality slider. Lower value means better quality.</source>
        <translation>质量滑条，更小的值意味着更好的质量。</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>打开文档</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>选择文件</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperResult</name>
    <message>
        <source>An error occurred!</source>
        <translation>发生错误！</translation>
    </message>
    <message>
        <source>Copy text to clipboard</source>
        <translation>复制到剪贴板</translation>
    </message>
    <message>
        <source>Back to create and send an error report!</source>
        <translation>返回以创建并发送错误报告！</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperVideoImportConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>生成预览图...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>生成预览缩略图...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>生成5秒预览视频...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>生成预览GIF...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>转换音频...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>转换视频... 这可能需要一些时间！</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>转换视频出错！</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>分析视频出错！</translation>
    </message>
    <message>
        <source>Convert a video to a wallpaper</source>
        <translation>将视频转换为壁纸</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>生成预览视频...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>名称（必选）</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>简介</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>Youtube 链接</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>中止</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>保存壁纸到...</translation>
    </message>
</context>
<context>
    <name>DefaultVideoControls</name>
    <message>
        <source>Volume</source>
        <translation>音量</translation>
    </message>
    <message>
        <source>Current Video Time</source>
        <translation>目前视频时间</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation>填充模式</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>拉伸</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>填充</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>适应</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>平铺</translation>
    </message>
    <message>
        <source>Scale_Down</source>
        <translation>裁切</translation>
    </message>
</context>
<context>
    <name>ExitPopup</name>
    <message>
        <source>Minimize ScreenPlay</source>
        <translation type="unfinished">Minimize ScreenPlay</translation>
    </message>
    <message>
        <source>Always minimize ScreenPlay</source>
        <translation type="unfinished">Always minimize ScreenPlay</translation>
    </message>
    <message>
        <source>You have active Wallpaper.
ScreenPlay will only quit if no Wallpaper are running.</source>
        <translation type="unfinished">You have active Wallpaper.
ScreenPlay will only quit if no Wallpaper are running.</translation>
    </message>
    <message>
        <source>You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the bottom right Tray-Icon.</source>
        <translation type="unfinished">You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the bottom right Tray-Icon.</translation>
    </message>
    <message>
        <source>You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the top right Tray-Icon.</source>
        <translation type="unfinished">You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the top right Tray-Icon.</translation>
    </message>
    <message>
        <source>Quit ScreenPlay now</source>
        <translation type="unfinished">Quit ScreenPlay now</translation>
    </message>
</context>
<context>
    <name>GifWallpaper</name>
    <message>
        <source>Import a Gif Wallpaper</source>
        <translation>导入 GIF 壁纸</translation>
    </message>
    <message>
        <source>Drop a *.gif file here or use &apos;Select file&apos; below.</source>
        <translation>拖放一个 *.gif 文件到这里，或者使用下面的 &apos;选择文件&apos;。</translation>
    </message>
    <message>
        <source>Select your gif</source>
        <translation>选择您的 GIF</translation>
    </message>
    <message>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>壁纸名称</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>作者</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>标签</translation>
    </message>
</context>
<context>
    <name>HTMLWallpaper</name>
    <message>
        <source>Create a HTML Wallpaper</source>
        <translation>创建 HTML 壁纸</translation>
    </message>
    <message>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>壁纸名称</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>作者</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>简介</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation>许可证 &amp; 标签</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>预览图</translation>
    </message>
</context>
<context>
    <name>HTMLWidget</name>
    <message>
        <source>Create a HTML widget</source>
        <translation>创建 HTML 部件</translation>
    </message>
    <message>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation>部件名称</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation>作者</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>标签</translation>
    </message>
</context>
<context>
    <name>ImportWebmConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>AnalyseVideo...</source>
        <translation>分析视频...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>生成预览图...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>生成预览缩略图...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>生成5秒预览视频...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>生成预览 GIF...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>转换音频...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>转换视频... 这可能需要一些时间！</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>转换视频出错！</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>分析视频出错！</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation>将视频导入为壁纸</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>生成预览视频...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>名称（必选）</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>简介</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>Youtube 链接</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>中止</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>保存壁纸...</translation>
    </message>
</context>
<context>
    <name>ImportWebmInit</name>
    <message>
        <source>Import a .webm video</source>
        <translation>导入 .webm 视频</translation>
    </message>
    <message>
        <source>When importing webm we can skip the long conversion. When you get unsatisfying results with the ScreenPlay importer from &apos;ideo import and convert (all types)&apos; you can also convert via the free and open source HandBrake!</source>
        <translation>导入webm时，我们可以跳过漫长的转换过程。当您对&apos;导入和转换（所有类型）&apos; ScreenPlay 得到的结果不满意时，您还可以使用免费开源的HandBrake!</translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid VP8 or VP9 (*.webm)!</source>
        <translation>文件类型无效。必须是 VP8 / VP9（*.webm）！</translation>
    </message>
    <message>
        <source>Drop a *.webm file here or use &apos;Select file&apos; below.</source>
        <translation>将一个webm文件拖到这里，或者使用下面的&apos;选择文件&apos;</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>打开文档</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>选择文件</translation>
    </message>
</context>
<context>
    <name>Importh264Convert</name>
    <message>
        <source>AnalyseVideo...</source>
        <translation>分析视频...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>生成预览图...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>生成预览缩略图...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>生成5秒预览视频...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>生成预览GIF...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>正在转换音频...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>正在转换视频... 这可能需要一些时间！</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>转换视频出错！</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>分析视频出错！</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation>将视频导入为壁纸</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>生成预览视频...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>名称 （必填）</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>简介</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>Youtube 链接</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>中止</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>保存壁纸...</translation>
    </message>
</context>
<context>
    <name>Importh264Init</name>
    <message>
        <source>Import a .mp4 video</source>
        <translation>导入 .mp4 视频</translation>
    </message>
    <message>
        <source>ScreenPlay V0.15 and up can play *.mp4 (also more known as h264). This can improove performance on older systems.</source>
        <translation>ScreenPlay V0.15及以上可播放 *.mp4 (通常称为h264)，这可以提高旧系统上的性能。</translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid h264 (*.mp4)!</source>
        <translation>文件类型无效。必须是有效的 h264 (*.mp4)！</translation>
    </message>
    <message>
        <source>Drop a *.mp4 file here or use &apos;Select file&apos; below.</source>
        <translation>将*.mp4文件拖入此处，或使用下面的 &apos;选择文件&apos;。</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>打开文档</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>选择文件</translation>
    </message>
</context>
<context>
    <name>Installed</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Refreshing!</source>
        <translation>刷新中！</translation>
    </message>
    <message>
        <source>Pull to refresh!</source>
        <translation>下拉以刷新！</translation>
    </message>
    <message>
        <source>Get more Wallpaper &amp; Widgets via the Steam workshop!</source>
        <translation>从创意工坊获取更多壁纸和物件！</translation>
    </message>
    <message>
        <source>Open containing folder</source>
        <translation>打开文件夹</translation>
    </message>
    <message>
        <source>Remove Item</source>
        <translation>删除物品</translation>
    </message>
    <message>
        <source>Remove via Workshop</source>
        <translation>从创意工坊中删除</translation>
    </message>
    <message>
        <source>Open Workshop Page</source>
        <translation>打开创意工坊页面</translation>
    </message>
    <message>
        <source>Are you sure you want to delete this item?</source>
        <translation>您确定要删除此物品？</translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished">Export</translation>
    </message>
    <message>
        <source>We only support adding one item at once.</source>
        <translation type="unfinished">We only support adding one item at once.</translation>
    </message>
    <message>
        <source>File type not supported. We only support &apos;.screenplay&apos; files.</source>
        <translation type="unfinished">File type not supported. We only support &apos;.screenplay&apos; files.</translation>
    </message>
    <message>
        <source>Import Content...</source>
        <translation type="unfinished">Import Content...</translation>
    </message>
    <message>
        <source>Export Content...</source>
        <translation type="unfinished">Export Content...</translation>
    </message>
</context>
<context>
    <name>InstalledNavigation</name>
    <message>
        <source>All</source>
        <translation type="unfinished">All</translation>
    </message>
    <message>
        <source>Scenes</source>
        <translation type="unfinished">Scenes</translation>
    </message>
    <message>
        <source>Videos</source>
        <translation type="unfinished">Videos</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation type="unfinished">Widgets</translation>
    </message>
</context>
<context>
    <name>InstalledWelcomeScreen</name>
    <message>
        <source>Get free Widgets and Wallpaper via the Steam Workshop</source>
        <translation>从创意工坊免费获取物件和壁纸</translation>
    </message>
    <message>
        <source>Browse the Steam Workshop</source>
        <translation>浏览创意工坊</translation>
    </message>
    <message>
        <source>Get content via our forum</source>
        <translation>通过论坛获取内容</translation>
    </message>
    <message>
        <source>Open the ScreenPlay forum</source>
        <translation type="unfinished">Open the ScreenPlay forum</translation>
    </message>
</context>
<context>
    <name>Monitors</name>
    <message>
        <source>Wallpaper Configuration</source>
        <translation>壁纸配置</translation>
    </message>
    <message>
        <source>Remove selected</source>
        <translation>移除已选择</translation>
    </message>
    <message>
        <source>Wallpapers</source>
        <translation>壁纸</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation>物件</translation>
    </message>
    <message>
        <source>Remove all </source>
        <translation>移除所有</translation>
    </message>
</context>
<context>
    <name>MonitorsProjectSettingItem</name>
    <message>
        <source>Set color</source>
        <translation>设置颜色</translation>
    </message>
    <message>
        <source>Please choose a color</source>
        <translation>请选择颜色</translation>
    </message>
</context>
<context>
    <name>Navigation</name>
    <message>
        <source>Create</source>
        <translation>创建</translation>
    </message>
    <message>
        <source>Workshop</source>
        <translation>创意工坊</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>已安装</translation>
    </message>
    <message>
        <source>Community</source>
        <translation>社区</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <source>Mute/Unmute all Wallpaper</source>
        <translation>静音/取消静音所有壁纸</translation>
    </message>
    <message>
        <source>Pause/Play all Wallpaper</source>
        <translation>暂停/播放所有壁纸</translation>
    </message>
    <message>
        <source>Configure Wallpaper</source>
        <translation>配置壁纸</translation>
    </message>
    <message>
        <source>Support me on Patreon!</source>
        <translation>在Patreon上支持！</translation>
    </message>
    <message>
        <source>Close All Content</source>
        <translation>关闭所有内容</translation>
    </message>
</context>
<context>
    <name>QMLWallpaper</name>
    <message>
        <source>Create a QML Wallpaper</source>
        <translation>创建一个QML壁纸</translation>
    </message>
    <message>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>壁纸名</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>作者</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>简介</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation>许可证 &amp; 标签</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>预览图</translation>
    </message>
</context>
<context>
    <name>QMLWallpaperMain</name>
    <message>
        <source>My ScreenPlay Wallpaper 🚀</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMLWidget</name>
    <message>
        <source>Create a QML widget</source>
        <translation>创建一个QML部件</translation>
    </message>
    <message>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation>部件名</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation>作者</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>标签</translation>
    </message>
</context>
<context>
    <name>QMLWidgetMain</name>
    <message>
        <source>My Widget 🚀</source>
        <translation type="unfinished">My Widget 🚀</translation>
    </message>
</context>
<context>
    <name>SaveNotification</name>
    <message>
        <source>Profile saved successfully!</source>
        <translation>配置保存成功！</translation>
    </message>
</context>
<context>
    <name>ScreenPlayItem</name>
    <message>
        <source>NEW</source>
        <translation>新</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <source>Autostart</source>
        <translation>自启动</translation>
    </message>
    <message>
        <source>ScreenPlay will start with Windows and will setup your Desktop every time for you.</source>
        <translation>ScreenPlay将在操作系统启动时启动，并会设置您的桌面。</translation>
    </message>
    <message>
        <source>High priority Autostart</source>
        <translation>高优先级自启动</translation>
    </message>
    <message>
        <source>This options grants ScreenPlay a higher autostart priority than other apps.</source>
        <translation>这个选项赋予ScreenPlay比其他应用程序更高的自启动优先级。</translation>
    </message>
    <message>
        <source>Send anonymous crash reports and statistics</source>
        <translation>发送匿名崩溃报告和统计数据</translation>
    </message>
    <message>
        <source>Help us make ScreenPlay faster and more stable. All collected data is purely anonymous and only used for development purposes! We use &lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt; to collect and analyze this data. A &lt;b&gt;big thanks to them&lt;/b&gt; for providing us with free premium support for open source projects!</source>
        <translation>帮助我们让 ScreenPlay 更快更稳定。所有被收集的数据完全匿名，而且仅用于开发用途！我们使用&lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt; 收集与分析数据。&lt;b&gt;感谢他们&lt;/b&gt; 为我们提供对开源项目免费而优质的服务！</translation>
    </message>
    <message>
        <source>Set save location</source>
        <translation>设置保存位置</translation>
    </message>
    <message>
        <source>Set location</source>
        <translation>选择位置</translation>
    </message>
    <message>
        <source>Your storage path is empty!</source>
        <translation>您的存储路径是空的！</translation>
    </message>
    <message>
        <source>Important: Changing this directory has no effect on the workshop download path. ScreenPlay only supports having one content folder!</source>
        <translation>注意：修改此目录并不影响创意工坊的下载路径。ScreenPlay仅支持单个内容文件夹！</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>语言</translation>
    </message>
    <message>
        <source>Set the ScreenPlay UI Language</source>
        <translation>设置ScreenPlay界面语言</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <source>Switch dark/light theme</source>
        <translation>切换到暗/亮主题</translation>
    </message>
    <message>
        <source>System Default</source>
        <translation>跟随系统</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>暗</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>亮</translation>
    </message>
    <message>
        <source>Performance</source>
        <translation>性能</translation>
    </message>
    <message>
        <source>Pause wallpaper video rendering while another app is in the foreground</source>
        <translation>当其他应用程序在前台时，暂停壁纸视频渲染</translation>
    </message>
    <message>
        <source>We disable the video rendering (not the audio!) for the best performance. If you have problem you can disable this behaviour here. Wallpaper restart required!</source>
        <translation>我们禁用视频渲染（不是音频）以获得最佳性能。如果您有问题，可以在此处禁用此行为。 需要重启壁纸！</translation>
    </message>
    <message>
        <source>Default Fill Mode</source>
        <translation>默认填充模式</translation>
    </message>
    <message>
        <source>Set this property to define how the video is scaled to fit the target area.</source>
        <translation>设置此属性可定义视频的缩放方式以适应目标区域。</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>拉伸</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>填充</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>适应</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>平铺</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation>裁剪</translation>
    </message>
    <message>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <source>Thank you for using ScreenPlay</source>
        <translation>感谢您的使用</translation>
    </message>
    <message>
        <source>Hi, I&apos;m Elias Steurer also known as Kelteseth and I&apos;m the developer of ScreenPlay. Thank you for using my software. You can follow me to receive updates about ScreenPlay here:</source>
        <translation>您好，我是Elias Steurer，也叫Kelteseth，我是ScreenPlay的开发者。感谢您使用我的软件。您可以在这里关注我，接收ScreenPlay的更新。</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <source>Open Changelog</source>
        <translation>打开更改日志。</translation>
    </message>
    <message>
        <source>Third Party Software</source>
        <translation>第三方软件</translation>
    </message>
    <message>
        <source>ScreenPlay would not be possible without the work of others. A big thank you to: </source>
        <translation>ScreenPlay离不开一些人的帮助。非常感谢你们：</translation>
    </message>
    <message>
        <source>Licenses</source>
        <translation>许可证</translation>
    </message>
    <message>
        <source>Logs</source>
        <translation>日志</translation>
    </message>
    <message>
        <source>If your ScreenPlay missbehaves this is a good way to look for answers. This shows all logs and warning during runtime.</source>
        <translation>如果您的ScreenPlay出错，这是个很好的查错方式。它显示所有的日志和运行时警告。</translation>
    </message>
    <message>
        <source>Show Logs</source>
        <translation>显示日志</translation>
    </message>
    <message>
        <source>Data Protection</source>
        <translation>数据保护</translation>
    </message>
    <message>
        <source>We use you data very carefully to improve ScreenPlay. We do not sell or share this (anonymous) information with others!</source>
        <translation>我们使用您的数据提升ScreenPlay的体验。我们承诺不出售或分享这些匿名信息！</translation>
    </message>
    <message>
        <source>Privacy</source>
        <translation>隐私</translation>
    </message>
</context>
<context>
    <name>SettingsExpander</name>
    <message>
        <source>Copy text to clipboard</source>
        <translation>复制文本至剪贴板</translation>
    </message>
</context>
<context>
    <name>Sidebar</name>
    <message>
        <source>Set Wallpaper</source>
        <translation>设置壁纸</translation>
    </message>
    <message>
        <source>Set Widget</source>
        <translation>设置物件</translation>
    </message>
    <message>
        <source>Headline</source>
        <translation>标题</translation>
    </message>
    <message>
        <source>Select a Monitor to display the content</source>
        <translation>选择显示此内容的显示器</translation>
    </message>
    <message>
        <source>Set Volume</source>
        <translation>设置音量</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation>填充模式</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>拉伸</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>填充</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>适应</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>平铺</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation>裁剪</translation>
    </message>
</context>
<context>
    <name>StartInfo</name>
    <message>
        <source>Free tools to help you to create wallpaper</source>
        <translation>免费壁纸创建工具</translation>
    </message>
    <message>
        <source>Below you can find tools to create wallaper, beyond the tools that ScreenPlay provides for you!</source>
        <translation>下面是一些非ScreenPlay提供的壁纸创建工具</translation>
    </message>
</context>
<context>
    <name>WebsiteWallpaper</name>
    <message>
        <source>Create a Website Wallpaper</source>
        <translation>创建一个网站壁纸</translation>
    </message>
    <message>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>壁纸名</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>作者</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>简介</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>标签</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>预览图</translation>
    </message>
</context>
<context>
    <name>WizardPage</name>
    <message>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <source>Saving...</source>
        <translation>保存中...</translation>
    </message>
</context>
<context>
    <name>XMLNewsfeed</name>
    <message>
        <source>News &amp; Patchnotes</source>
        <translation>新闻和更改日志</translation>
    </message>
</context>
</TS>
