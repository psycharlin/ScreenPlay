<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT" sourcelanguage="en">
<context>
    <name>Community</name>
    <message>
        <source>News</source>
        <translation>Notizie</translation>
    </message>
    <message>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <source>Forum</source>
        <translation>Forum</translation>
    </message>
    <message>
        <source>Contribute</source>
        <translation>Contribuisci</translation>
    </message>
    <message>
        <source>Steam Workshop</source>
        <translation>Steam Workshop</translation>
    </message>
    <message>
        <source>Issue Tracker</source>
        <translation type="unfinished">Issue Tracker</translation>
    </message>
    <message>
        <source>Reddit</source>
        <translation type="unfinished">Reddit</translation>
    </message>
</context>
<context>
    <name>CommunityNavItem</name>
    <message>
        <source>Open in browser</source>
        <translation>Apri nel browser</translation>
    </message>
</context>
<context>
    <name>CreateSidebar</name>
    <message>
        <source>Tools Overview</source>
        <translation type="unfinished">Tools Overview</translation>
    </message>
    <message>
        <source>Video Import h264 (.mp4)</source>
        <translation type="unfinished">Video Import h264 (.mp4)</translation>
    </message>
    <message>
        <source>Video Import VP8 &amp; VP9 (.webm)</source>
        <translation type="unfinished">Video Import VP8 &amp; VP9 (.webm)</translation>
    </message>
    <message>
        <source>Video import (all types)</source>
        <translation type="unfinished">Video import (all types)</translation>
    </message>
    <message>
        <source>GIF Wallpaper</source>
        <translation type="unfinished">GIF Wallpaper</translation>
    </message>
    <message>
        <source>QML Wallpaper</source>
        <translation type="unfinished">QML Wallpaper</translation>
    </message>
    <message>
        <source>HTML5 Wallpaper</source>
        <translation type="unfinished">HTML5 Wallpaper</translation>
    </message>
    <message>
        <source>Website Wallpaper</source>
        <translation type="unfinished">Website Wallpaper</translation>
    </message>
    <message>
        <source>QML Widget</source>
        <translation type="unfinished">QML Widget</translation>
    </message>
    <message>
        <source>HTML Widget</source>
        <translation type="unfinished">HTML Widget</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperInit</name>
    <message>
        <source>Import any video type</source>
        <translation>Importa qualsiasi tipo di video</translation>
    </message>
    <message>
        <source>Depending on your PC configuration it is better to convert your wallpaper to a specific video codec. If both have bad performance you can also try a QML wallpaper! Supported video formats are: 

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</source>
        <translation>A seconda della configurazione del tuo PC è meglio comvertire lo sfondo in un codec specifico. Se entrambi hanno scarse performance puoi provare uno sfondo QML! I formati video supportati sono:

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</translation>
    </message>
    <message>
        <source>Set your preffered video codec:</source>
        <translation>Imposta il tuo codec video preferito:</translation>
    </message>
    <message>
        <source>Quality slider. Lower value means better quality.</source>
        <translation>Cursore per la qualità. A valori inferiori corrisponde una qualità migliore.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Apri documentazione</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Seleziona file</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperResult</name>
    <message>
        <source>An error occurred!</source>
        <translation>Si è verificato un errore!</translation>
    </message>
    <message>
        <source>Copy text to clipboard</source>
        <translation>Copia testo negli appunti</translation>
    </message>
    <message>
        <source>Back to create and send an error report!</source>
        <translation>Torna a creare e inviare un report di errore!</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperVideoImportConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Generazione anteprima immagine...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Generazione miniatura di anteprima...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>Generazione anteprima video di 5 secondi...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Generazione gif di anteprima...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Conversione Audio...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Conversione Video... Potrebbe richiedere un po&apos; di tempo!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>Conversione Video ERRORE!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>Analisi Video ERRORE!</translation>
    </message>
    <message>
        <source>Convert a video to a wallpaper</source>
        <translation>Converti un video in uno sfondo</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Generazione video di anteprima...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>Nome (obbligatorio)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>URL Youtube</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Interrompi</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Salva sfondo...</translation>
    </message>
</context>
<context>
    <name>DefaultVideoControls</name>
    <message>
        <source>Volume</source>
        <translation>Volume</translation>
    </message>
    <message>
        <source>Current Video Time</source>
        <translation>Minutaggio</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation>Modalità Riempimento</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Estensione</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Riempimento schermo</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Proporzioni mantenute (barre nere)</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Taglia</translation>
    </message>
    <message>
        <source>Scale_Down</source>
        <translation>Riduci</translation>
    </message>
</context>
<context>
    <name>ExitPopup</name>
    <message>
        <source>Minimize ScreenPlay</source>
        <translation type="unfinished">Minimize ScreenPlay</translation>
    </message>
    <message>
        <source>Always minimize ScreenPlay</source>
        <translation type="unfinished">Always minimize ScreenPlay</translation>
    </message>
    <message>
        <source>You have active Wallpaper.
ScreenPlay will only quit if no Wallpaper are running.</source>
        <translation type="unfinished">You have active Wallpaper.
ScreenPlay will only quit if no Wallpaper are running.</translation>
    </message>
    <message>
        <source>You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the bottom right Tray-Icon.</source>
        <translation type="unfinished">You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the bottom right Tray-Icon.</translation>
    </message>
    <message>
        <source>You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the top right Tray-Icon.</source>
        <translation type="unfinished">You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the top right Tray-Icon.</translation>
    </message>
    <message>
        <source>Quit ScreenPlay now</source>
        <translation type="unfinished">Quit ScreenPlay now</translation>
    </message>
</context>
<context>
    <name>GifWallpaper</name>
    <message>
        <source>Import a Gif Wallpaper</source>
        <translation>Importa uno sfondo Gif</translation>
    </message>
    <message>
        <source>Drop a *.gif file here or use &apos;Select file&apos; below.</source>
        <translation>Trascina un file *.gif qui o usa &apos;Seleziona file&apos; qui sotto.</translation>
    </message>
    <message>
        <source>Select your gif</source>
        <translation>Seleziona la tua gif</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Nome sfondo</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Creato da</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Tag</translation>
    </message>
</context>
<context>
    <name>HTMLWallpaper</name>
    <message>
        <source>Create a HTML Wallpaper</source>
        <translation>Crea uno sfondo HTML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Nome sfondo</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Creato da</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation>Licenza &amp; Tag</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Immagine d’anteprima</translation>
    </message>
</context>
<context>
    <name>HTMLWidget</name>
    <message>
        <source>Create a HTML widget</source>
        <translation>Crea un Widget HTML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation>Nome Widget</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation>Creato da</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Tag</translation>
    </message>
</context>
<context>
    <name>ImportWebmConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>AnalyseVideo...</source>
        <translation>Analizza Video...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Generazione immagine d&apos;anteprima...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Generazione miniatura di anteprima...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>Generazione anteprima video di 5 secondi...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Generazione gif di anteprima...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Conversione Audio...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Conversione Video... Potrebbe richiedere un po&apos; di tempo!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>Conversione Video ERRORE!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>Analisi Video ERRORE!</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation>Importa un video in uno sfondo</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Generazione video di anteprima...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>Nome (obbligatorio)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>URL Youtube</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Interrompi</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Salva sfondo...</translation>
    </message>
</context>
<context>
    <name>ImportWebmInit</name>
    <message>
        <source>Import a .webm video</source>
        <translation>Importa un video .webm</translation>
    </message>
    <message>
        <source>When importing webm we can skip the long conversion. When you get unsatisfying results with the ScreenPlay importer from &apos;ideo import and convert (all types)&apos; you can also convert via the free and open source HandBrake!</source>
        <translation>Importando un file webm è possibile saltare la conversione. Se ottiene risultati insoddisfacenti dall&apos;importer di ScreenPlay &apos;Importa e converti video (tutti i tipi)&apos; puoi anche farlo con il convertitore gratuito ed open source HandBrake!</translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid VP8 or VP9 (*.webm)!</source>
        <translation>Tipo di file non valido. Deve essere un valido VP8 o VP9 (*.webm)!</translation>
    </message>
    <message>
        <source>Drop a *.webm file here or use &apos;Select file&apos; below.</source>
        <translation>Trascina un file *.webm qui o usa &apos;Seleziona file&apos; qui sotto.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Apri documentazione</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Seleziona file</translation>
    </message>
</context>
<context>
    <name>Importh264Convert</name>
    <message>
        <source>AnalyseVideo...</source>
        <translation type="unfinished">AnalyseVideo...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation type="unfinished">Generating preview image...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation type="unfinished">Generating preview thumbnail image...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation type="unfinished">Generating 5 second preview video...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation type="unfinished">Generating preview gif...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation type="unfinished">Converting Audio...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation type="unfinished">Converting Video... This can take some time!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation type="unfinished">Converting Video ERROR!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation type="unfinished">Analyse Video ERROR!</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation type="unfinished">Import a video to a wallpaper</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation type="unfinished">Generating preview video...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation type="unfinished">Name (required!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">Description</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation type="unfinished">Youtube URL</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="unfinished">Abort</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished">Save</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation type="unfinished">Save Wallpaper...</translation>
    </message>
</context>
<context>
    <name>Importh264Init</name>
    <message>
        <source>Import a .mp4 video</source>
        <translation type="unfinished">Import a .mp4 video</translation>
    </message>
    <message>
        <source>ScreenPlay V0.15 and up can play *.mp4 (also more known as h264). This can improove performance on older systems.</source>
        <translation type="unfinished">ScreenPlay V0.15 and up can play *.mp4 (also more known as h264). This can improove performance on older systems.</translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid h264 (*.mp4)!</source>
        <translation type="unfinished">Invalid file type. Must be valid h264 (*.mp4)!</translation>
    </message>
    <message>
        <source>Drop a *.mp4 file here or use &apos;Select file&apos; below.</source>
        <translation type="unfinished">Drop a *.mp4 file here or use &apos;Select file&apos; below.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation type="unfinished">Open Documentation</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation type="unfinished">Select file</translation>
    </message>
</context>
<context>
    <name>Installed</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Refreshing!</source>
        <translation>Aggiornamento!</translation>
    </message>
    <message>
        <source>Pull to refresh!</source>
        <translation>Trascina per aggiornare!</translation>
    </message>
    <message>
        <source>Get more Wallpaper &amp; Widgets via the Steam workshop!</source>
        <translation>Ottieni altri Wallpaper &amp; Widget dal workshop di Steam!</translation>
    </message>
    <message>
        <source>Open containing folder</source>
        <translation>Apri percorso file</translation>
    </message>
    <message>
        <source>Remove Item</source>
        <translation>Rimuovi elemento</translation>
    </message>
    <message>
        <source>Remove via Workshop</source>
        <translation>Rimuovi tramite Workshop</translation>
    </message>
    <message>
        <source>Open Workshop Page</source>
        <translation>Apri pagina del Workshop</translation>
    </message>
    <message>
        <source>Are you sure you want to delete this item?</source>
        <translation>Sei sicuro di voler rimuovere questo elemento?</translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished">Export</translation>
    </message>
    <message>
        <source>We only support adding one item at once.</source>
        <translation type="unfinished">We only support adding one item at once.</translation>
    </message>
    <message>
        <source>File type not supported. We only support &apos;.screenplay&apos; files.</source>
        <translation type="unfinished">File type not supported. We only support &apos;.screenplay&apos; files.</translation>
    </message>
    <message>
        <source>Import Content...</source>
        <translation type="unfinished">Import Content...</translation>
    </message>
    <message>
        <source>Export Content...</source>
        <translation type="unfinished">Export Content...</translation>
    </message>
</context>
<context>
    <name>InstalledNavigation</name>
    <message>
        <source>All</source>
        <translation type="unfinished">All</translation>
    </message>
    <message>
        <source>Scenes</source>
        <translation type="unfinished">Scenes</translation>
    </message>
    <message>
        <source>Videos</source>
        <translation type="unfinished">Videos</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation type="unfinished">Widgets</translation>
    </message>
</context>
<context>
    <name>InstalledWelcomeScreen</name>
    <message>
        <source>Get free Widgets and Wallpaper via the Steam Workshop</source>
        <translation>Ottieni Widget e Wallpaper gratuiti tramite il Steam Workshop</translation>
    </message>
    <message>
        <source>Browse the Steam Workshop</source>
        <translation>Sfoglia lo Steam Workshop</translation>
    </message>
    <message>
        <source>Get content via our forum</source>
        <translation type="unfinished">Get content via our forum</translation>
    </message>
    <message>
        <source>Open the ScreenPlay forum</source>
        <translation type="unfinished">Open the ScreenPlay forum</translation>
    </message>
</context>
<context>
    <name>Monitors</name>
    <message>
        <source>Wallpaper Configuration</source>
        <translation>Configurazione Sfondo</translation>
    </message>
    <message>
        <source>Remove selected</source>
        <translation>Rimuovi selezionati</translation>
    </message>
    <message>
        <source>Wallpapers</source>
        <translation>Sfondi</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation>Widgets</translation>
    </message>
    <message>
        <source>Remove all </source>
        <translation type="unfinished">Remove all </translation>
    </message>
</context>
<context>
    <name>MonitorsProjectSettingItem</name>
    <message>
        <source>Set color</source>
        <translation>Imposta colore</translation>
    </message>
    <message>
        <source>Please choose a color</source>
        <translation>Scegli un colore</translation>
    </message>
</context>
<context>
    <name>Navigation</name>
    <message>
        <source>Create</source>
        <translation>Crea</translation>
    </message>
    <message>
        <source>Workshop</source>
        <translation>Workshop</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>Installati</translation>
    </message>
    <message>
        <source>Community</source>
        <translation>Comunità</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <source>Mute/Unmute all Wallpaper</source>
        <translation type="unfinished">Mute/Unmute all Wallpaper</translation>
    </message>
    <message>
        <source>Pause/Play all Wallpaper</source>
        <translation type="unfinished">Pause/Play all Wallpaper</translation>
    </message>
    <message>
        <source>Configure Wallpaper</source>
        <translation type="unfinished">Configure Wallpaper</translation>
    </message>
    <message>
        <source>Support me on Patreon!</source>
        <translation type="unfinished">Support me on Patreon!</translation>
    </message>
    <message>
        <source>Close All Content</source>
        <translation type="unfinished">Close All Content</translation>
    </message>
</context>
<context>
    <name>QMLWallpaper</name>
    <message>
        <source>Create a QML Wallpaper</source>
        <translation>Crea uno sfondo QML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Nome sfondo</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Creato da</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation>Licenza &amp; Tag</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Immagine d’anteprima</translation>
    </message>
</context>
<context>
    <name>QMLWallpaperMain</name>
    <message>
        <source>My ScreenPlay Wallpaper 🚀</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMLWidget</name>
    <message>
        <source>Create a QML widget</source>
        <translation>Crea un Widget QML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation>Nome Widget</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation>Creato da</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Tag</translation>
    </message>
</context>
<context>
    <name>QMLWidgetMain</name>
    <message>
        <source>My Widget 🚀</source>
        <translation type="unfinished">My Widget 🚀</translation>
    </message>
</context>
<context>
    <name>SaveNotification</name>
    <message>
        <source>Profile saved successfully!</source>
        <translation>Prodilo salvato correttamente!</translation>
    </message>
</context>
<context>
    <name>ScreenPlayItem</name>
    <message>
        <source>NEW</source>
        <translation>NUOVO</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <source>Autostart</source>
        <translation>Avvio automatico</translation>
    </message>
    <message>
        <source>ScreenPlay will start with Windows and will setup your Desktop every time for you.</source>
        <translation>ScreenPlay si avvierà con Windows e configurerà il desktop ogni volta per te.</translation>
    </message>
    <message>
        <source>High priority Autostart</source>
        <translation>Avvio automatico ad alta priorità</translation>
    </message>
    <message>
        <source>This options grants ScreenPlay a higher autostart priority than other apps.</source>
        <translation>Questa opzione garantisce a ScreenPlay una priorità di avvio automatico più alta di altre applicazioni.</translation>
    </message>
    <message>
        <source>Send anonymous crash reports and statistics</source>
        <translation>Invia report e statistiche anonime sui crash</translation>
    </message>
    <message>
        <source>Help us make ScreenPlay faster and more stable. All collected data is purely anonymous and only used for development purposes! We use &lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt; to collect and analyze this data. A &lt;b&gt;big thanks to them&lt;/b&gt; for providing us with free premium support for open source projects!</source>
        <translation>Aiutaci a rendere ScreenPlay più veloce e stabile. Tutti i dati raccolti sono puramente anonimi e utilizzati solo per scopi di sviluppo! Usiamo &lt;a href=&quot;https://sentry.io&quot;&gt;sentry. o&lt;/a&gt; per raccogliere e analizzare questi dati. Un &lt;b&gt;grande grazie a loro&lt;/b&gt; per averci fornito supporto premium gratuito per i progetti open source!</translation>
    </message>
    <message>
        <source>Set save location</source>
        <translation>Imposta cartella di salvataggio</translation>
    </message>
    <message>
        <source>Set location</source>
        <translation>Imposta posizione</translation>
    </message>
    <message>
        <source>Your storage path is empty!</source>
        <translation>Il tuo percorso di archiviazione è vuoto!</translation>
    </message>
    <message>
        <source>Important: Changing this directory has no effect on the workshop download path. ScreenPlay only supports having one content folder!</source>
        <translation>Importante: la modifica di questa cartella non ha effetto sul percorso di download del workshop. ScreenPlay supporta solo una cartella contenuti!</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Lingua</translation>
    </message>
    <message>
        <source>Set the ScreenPlay UI Language</source>
        <translation>Imposta la lingua dell&apos;interfaccia utente di ScreenPlay</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Switch dark/light theme</source>
        <translation>Cambia tema scuro/chiaro</translation>
    </message>
    <message>
        <source>System Default</source>
        <translation>Predefinito di Sistema</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Scuro</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Chiaro</translation>
    </message>
    <message>
        <source>Performance</source>
        <translation>Prestazioni</translation>
    </message>
    <message>
        <source>Pause wallpaper video rendering while another app is in the foreground</source>
        <translation>Metti in pausa il rendering video dello sfondo mentre un&apos;altra app è in primo piano</translation>
    </message>
    <message>
        <source>We disable the video rendering (not the audio!) for the best performance. If you have problem you can disable this behaviour here. Wallpaper restart required!</source>
        <translation>Disattiviamo il rendering video (non l&apos;audio!) per migliori prestazioni. Se hai problemi puoi disabilitare questa opzione qui. È necessario riavviare lo sfondo!</translation>
    </message>
    <message>
        <source>Default Fill Mode</source>
        <translation>Modalità riempimento predefinita</translation>
    </message>
    <message>
        <source>Set this property to define how the video is scaled to fit the target area.</source>
        <translation>Imposta questa proprietà per definire come il video viene adattato all&apos;area di destinazione.</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Estensione</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Riempimento</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Contenimento</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Coprente</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation>Riduzione</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Informazioni aggiuntive</translation>
    </message>
    <message>
        <source>Thank you for using ScreenPlay</source>
        <translation>Grazie per aver usato ScreenPlay</translation>
    </message>
    <message>
        <source>Hi, I&apos;m Elias Steurer also known as Kelteseth and I&apos;m the developer of ScreenPlay. Thank you for using my software. You can follow me to receive updates about ScreenPlay here:</source>
        <translation>Ciao, sono Elias Steurer conosciuto anche come Kelteseth e sono lo sviluppatore di ScreenPlay. Grazie per aver utilizzato il mio software. Puoi seguirmi per ricevere aggiornamenti su ScreenPlay qui:</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Versione</translation>
    </message>
    <message>
        <source>Open Changelog</source>
        <translation>Apri Changelog</translation>
    </message>
    <message>
        <source>Third Party Software</source>
        <translation>Software di terze parti</translation>
    </message>
    <message>
        <source>ScreenPlay would not be possible without the work of others. A big thank you to: </source>
        <translation>ScreenPlay non sarebbe possibile senza il lavoro di altri. Un grande ringraziamento a: </translation>
    </message>
    <message>
        <source>Licenses</source>
        <translation>Licenze</translation>
    </message>
    <message>
        <source>Logs</source>
        <translation>Logs</translation>
    </message>
    <message>
        <source>If your ScreenPlay missbehaves this is a good way to look for answers. This shows all logs and warning during runtime.</source>
        <translation>Se il tuo ScreenPlay non funziona correttamente questo è un buon modo per cercare risposte. Questo mostra tutti i log e gli avvisi durante l&apos;esecuzione.</translation>
    </message>
    <message>
        <source>Show Logs</source>
        <translation>Visualizza i Log</translation>
    </message>
    <message>
        <source>Data Protection</source>
        <translation>Protezione dei dati</translation>
    </message>
    <message>
        <source>We use you data very carefully to improve ScreenPlay. We do not sell or share this (anonymous) information with others!</source>
        <translation>Utilizziamo i tuoi dati con molta attenzione per migliorare ScreenPlay. Non vendiamo o condividiamo queste informazioni (anonime) con altri!</translation>
    </message>
    <message>
        <source>Privacy</source>
        <translation>Privacy</translation>
    </message>
</context>
<context>
    <name>SettingsExpander</name>
    <message>
        <source>Copy text to clipboard</source>
        <translation>Copia testo negli appunti</translation>
    </message>
</context>
<context>
    <name>Sidebar</name>
    <message>
        <source>Set Wallpaper</source>
        <translation>Imposta sfondo</translation>
    </message>
    <message>
        <source>Set Widget</source>
        <translation>Imposta Widget</translation>
    </message>
    <message>
        <source>Headline</source>
        <translation>Intestazione</translation>
    </message>
    <message>
        <source>Select a Monitor to display the content</source>
        <translation>Seleziona un monitor per visualizzare il contenuto</translation>
    </message>
    <message>
        <source>Set Volume</source>
        <translation>Imposta volume</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation>Modalità Riempimento</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Estensione</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Riempimento</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Proporzioni mantenute (barre nere)</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Taglia</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation>Rimpicciolisci</translation>
    </message>
</context>
<context>
    <name>StartInfo</name>
    <message>
        <source>Free tools to help you to create wallpaper</source>
        <translation type="unfinished">Free tools to help you to create wallpaper</translation>
    </message>
    <message>
        <source>Below you can find tools to create wallaper, beyond the tools that ScreenPlay provides for you!</source>
        <translation type="unfinished">Below you can find tools to create wallaper, beyond the tools that ScreenPlay provides for you!</translation>
    </message>
</context>
<context>
    <name>WebsiteWallpaper</name>
    <message>
        <source>Create a Website Wallpaper</source>
        <translation>Crea uno sfondo web</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Nome sfondo</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Creato da</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Tag</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Immagine d’anteprima</translation>
    </message>
</context>
<context>
    <name>WizardPage</name>
    <message>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <source>Saving...</source>
        <translation>Salvataggio in corso...</translation>
    </message>
</context>
<context>
    <name>XMLNewsfeed</name>
    <message>
        <source>News &amp; Patchnotes</source>
        <translation>Notizie &amp; Patchnotes</translation>
    </message>
</context>
</TS>
